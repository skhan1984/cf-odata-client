const express = require("express");
const request = require("request");
const cfenv = require("cfenv");
const app = express();
//const uname = "infdev3";
//const pwd = "Infy@123";
//const url = "http://eassap1.ad.infosys.com:8003/sap/opu/odata/sap/ZVBELN_SERVICE_SRV/vbeln_detailSet('270')?$format=xml";
//const auth = "Basic " + new Buffer(uname + "@" + pwd).toString("base64");
const sEndpoint = "/sap/opu/odata/sap/ZVBELN_SERVICE_SRV/vbeln_detailSet('270')?$format=xml";
const oServices = cfenv.getAppEnv().getServices();
const uaaService = cfenv.getAppEnv().getService("uaaservice");
const destService = cfenv.getAppEnv().getService("destinationservice");
const connService = cfenv.getAppEnv().getService("connservice");
const sUaaCredentials = destService.credentials.clientid + ':' + destService.credentials.clientsecret;
const sUaaCredentialsConn = connService.credentials.clientid + ':' + connService.credentials.clientsecret;
const sDestinationName = "odatadestination";

const post_options = {
    url: uaaService.credentials.url + '/oauth/token',
    method: 'POST',
    headers: {
        'Authorization': 'Basic ' + Buffer.from(sUaaCredentials).toString('base64'),
        'Content-type': 'application/x-www-form-urlencoded'
    },
    form: {
        'client_id': destService.credentials.clientid,
        'grant_type': 'client_credentials'
    }
}

const conn_post_options = {
    url: connService.credentials.url + '/oauth/token',
    method: 'POST',
    headers: {
        'Authorization': 'Basic ' + Buffer.from(sUaaCredentialsConn).toString('base64'),
        'Content-type': 'application/x-www-form-urlencoded'
    },
    form: {
        'client_id': connService.credentials.clientid,
        'grant_type': 'client_credentials'
    }
}

app.get("/", function(expReq, expResp){	
	request(post_options, (authErr, authRes, authData)=> {
		if(authErr){
			expResp.send("Authentication failed " + JSON.stringify(authErr));
			return;
		}
		if(authRes.statusCode == 200){			
			const token = JSON.parse(authData).access_token;
			const get_options = {
				url: destService.credentials.uri + "/destination-configuration/v1/destinations/" + sDestinationName,
				headers: {"Authorization":"Bearer " + token}
			}			
			request(get_options, (destErr, destRes, destData) => {			
				if(destErr){
					expResp.send("Unable to get destination " + JSON.stringify(destErr));
					return;
				}
				request(conn_post_options, (connAuthErr, connAuthRes, connAuthData)=> {
					if(connAuthErr){
						expResp.send(" Connectivity authentication failed " + JSON.stringify(connAuthErr));
						return;
					}					
					const oConnAuthToken = JSON.parse(connAuthData).access_token;
					const oDestination = JSON.parse(destData);
					const destToken = oDestination.authTokens[0];
					const proxyURL = "http://" + connService.credentials.onpremise_proxy_host + ":" + connService.credentials.onpremise_proxy_port;					
					const odata_get = {
						url: oDestination.destinationConfiguration.URL + sEndpoint,
						proxy: proxyURL,
						headers: {
							"Authorization": `${destToken.type} ${destToken.value}`,
							"Proxy-Authorization": "Bearer " + oConnAuthToken
						}
					}				
					request(odata_get, (odataErr, odataRes, odataContent ) => {
						expResp.header('content-type', 'application/xml');
						expResp.send(odataContent);
					});
				});			
			});		
		}else{
			expResp.send("Unable to get access_token");
		}
	
	});
});

const port = process.env.PORT || 3000;
app.listen(port, function(){
	console.log("app listening on port " + port);
});